# simple_patch

![rendering of the PCB](render/rendering.png)

A simple breakout board / eurocrack module for the Daisy patch submodule

Based on the patch.init() 
 
https://electro-smith.com/products/patch-submodule

## Hardware refence design
https://daisy.nyc3.cdn.digitaloceanspaces.com/products/patch-init/ES_Daisy_Patch_SM_Init_Rev1.zip
 
 